ARG BASE_IMAGE_VERSION=1.0.4

# container
FROM registry.gitlab.com/luigi600/alpine-latex-inkscape:$BASE_IMAGE_VERSION

RUN apk add ghostscript

ADD pdfconverterCMYK.sh usr/bin/pdfconverterCMYK

RUN ["chmod", "+x", "usr/bin/pdfconverterCMYK"]

ENTRYPOINT ["pdfconverterCMYK"]
