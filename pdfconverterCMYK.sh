#!/bin/sh
output_file="${CMYK_FILE_NAME:-${1}_CMYK}.pdf"
pdfconverter "${1}"
gs -o "${output_file}" -sDEVICE=pdfwrite -sProcessColorModel=DeviceCMYK -sColorConversionStrategy=CMYK -sColorConversionStrategyForImages=CMYK "${1}.pdf"
